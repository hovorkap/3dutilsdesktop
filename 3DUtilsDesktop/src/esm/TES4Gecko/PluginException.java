package esm.TES4Gecko;

public class PluginException extends Exception
{
	public PluginException()
	{
	}

	public PluginException(String exceptionMsg)
	{
		super(exceptionMsg);
	}

	public PluginException(String exceptionMsg, Throwable cause)
	{
		super(exceptionMsg, cause);
	}
}

/* Location:           C:\temp\TES4Gecko\
 * Qualified Name:     TES4Gecko.PluginException
 * JD-Core Version:    0.6.0
 */